﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace irock.Math
{
    public static class Constants
    {
        public const float PI = 3.14159265358979f;

        public const float Tau = PI * 2;
    }
}
