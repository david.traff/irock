﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace irock.Hackbase.Generics
{
    public abstract class Hack : IHack
    {
        /// <summary>
        /// The hacks main update-method.
        /// </summary>
        public abstract void Update();

        private bool _Enabled;

        /// <summary>
        /// Dictates whether the hack should receive update-calls.
        /// </summary>
        public bool Enabled
        {
            get
            {
                return _Enabled;
            }
            set
            {
                _Enabled = value;
            }
        }
    }
}
