﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace irock.Hackbase.Generics
{
    /// <summary>
    /// Defines special options for hacks. This is what the plugin-manager will look for in your plugins.
    /// No HackDefinition, no hack.
    /// </summary>
    public class HackDefinition : Attribute
    {
        private string _Name;
        public string Name => _Name;

        private bool _Autostart;
        public bool Autostart => _Autostart;

        private int _UpdateFrequency;
        public int UpdateFrequency => _UpdateFrequency;

        private bool _RequiresOwnThread;
        public bool RequiresOwnThread => _RequiresOwnThread;

        /// <summary>
        /// Constructor for your hackdefinition.
        /// </summary>
        /// <param name="name">Name of the hack</param>
        /// <param name="autostart">If true the hack with autostart.</param>
        /// <param name="updateFrequency">Number of milliseconds between calls of the <see cref="IHack.Update"/> method</param>
        /// <param name="requiresOwnThread">If true the hack will have its <see cref="IHack.Update"/> calls run on a separate thread</param>
        public HackDefinition(string name, bool autostart = false, int updateFrequency = 10, bool requiresOwnThread = false)
        {
            _Name = name;
            _Autostart = autostart;
            _UpdateFrequency = updateFrequency;
            _RequiresOwnThread = requiresOwnThread;
        }
    }
}
