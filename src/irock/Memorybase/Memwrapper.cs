﻿using BlueRain;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace irock.Memorybase
{
    public static class Memwrapper
    {
        private static bool _Initialized;
        /// <summary>
        /// Is attatched to process
        /// </summary>
        public static bool Initialized => _Initialized;

        private static Process _Process;
        /// <summary>
        /// Associated process.
        /// </summary>
        public static Process Process => _Process;

        private static ExternalProcessMemory _Memory;
        /// <summary>
        /// Process' memory.
        /// </summary>
        public static ExternalProcessMemory Memory => _Memory;

        private static ProcessModule _EngineBase;
        public static ProcessModule EngineBase => _EngineBase;

        private static ProcessModule _ClientBase;
        public static ProcessModule ClientBase => _ClientBase;

        internal static void Write<T>(IntPtr address, IEntry entry) where T : struct
        {
            var val = (StateEntry<T>)entry;

            _Memory.Write<T>(address, val.Value);
        }

        public static bool Initialize(Process process)
        {
            _Initialized = false;
            _Process = process;

            _Memory = new ExternalProcessMemory(process);

            _EngineBase = _Memory.GetModule("engine.dll");
            _ClientBase = _Memory.GetModule("client.dll");



            return true;
        }
    }
}
