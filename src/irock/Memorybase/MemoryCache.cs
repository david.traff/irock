﻿using BlueRain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Reflection;

namespace irock.Memorybase
{
    /// <summary>
    /// Our hacks memory r/w will tunnel through this between updates. Main loop will later flush this to memory.
    /// Memorylocations will serve as unique keys. That may cause problems for some.
    /// </summary>
    public class MemoryCache
    {
        public MemoryCache()
        {
            _GenericCalls = new Dictionary<Type, MethodInfo>();
        }

        private static readonly object Lock = new object();

        /// <summary>
        /// Specifies whether <see cref="Read"/> will use locking to make avoid multiple reads of the same address.
        /// </summary>
        public bool PerformLocking { get; set; }

        public int WriteCalls => CurrentState.WriteCalls;
        public int ReadCalls => CurrentState.ReadCalls;
        public int TotalRWCalls => CurrentState.TotalRWCalls;

        private State CurrentState;

        //Collection of Type -> Generic MemWrapper.Write<T>
        private Dictionary<Type, MethodInfo> _GenericCalls;

        /// <summary>
        /// Begins to fill the internal memorystate.
        /// </summary>
        public void BeginFilling()
        {
            CurrentState = new State();
        }

        /// <summary>
        /// Flushes the diff to the process' memory.
        /// </summary>
        public void Flush()
        {
            var state = CurrentState.FlushState();

            for(int i = 0; i < state.Count; i++)
            {
                var kvp = state.ElementAt(i);
                var key = kvp.Key;
                var val = kvp.Value;
                var typ = val.GetType().GetGenericArguments()[0];

                if (!val.HasWritten)
                    continue;

                //I'd gladly do this in another way - maybe not do stateentry generic?
                MethodInfo method;
                if (_GenericCalls.ContainsKey(typ))
                {
                    method = _GenericCalls[typ];
                }
                else
                {
                    MethodInfo m = typeof(Memwrapper).GetMethod("Write");
                    MethodInfo generic = m.MakeGenericMethod(typ);

                    _GenericCalls.Add(typ, generic);
                    method = generic;
                }

                method.Invoke(Memwrapper.Memory, new object[] { key, val });
            }
        }

        /// <summary>
        /// Reads <typeparamref name="T"/> from <paramref name="address"/>
        /// </summary>
        /// <typeparam name="T">Struct to be read</typeparam>
        /// <param name="address">Address to read <typeparamref name="T"/> at</param>
        /// <returns></returns>
        public T Read<T>(IntPtr address) where T : struct
        {
            if (PerformLocking)
            {
                lock (Lock)
                {
                    return _Read<T>(address);
                }
            }
            else
                return _Read<T>(address);
        }

        private T _Read<T>(IntPtr address) where T : struct
        {
            if (address == IntPtr.Zero)
                return default(T);

            StateEntry<T> val;
            if (CurrentState.GetIfExists<T>(address, out val))
                return val.Value;
            else
            {
                val = new StateEntry<T>()
                {
                    HasWritten = false,
                    Type = typeof(T)
                };
                val.Value = Memwrapper.Memory.Read<T>(address);

                CurrentState.AddOrUpdate(address, val, false);

                return val.Value;
            }
        }

        public void Write<T>(IntPtr address, T value) where T : struct
        {
            StateEntry<T> val;
            if (CurrentState.GetIfExists<T>(address, out val))
            {
                val.HasWritten = true;
                val.Value = value;
            }
            else
            {
                val = new StateEntry<T>()
                {
                    HasWritten = true,
                    Type = typeof(T),
                    Value = value
                };

                CurrentState.AddOrUpdate(address, val, true);
            }
        }

        private class State
        {
            private int _WriteCalls;
            /// <summary>
            /// Number of Write-calls hacks have done during this states lifetime
            /// </summary>
            public int WriteCalls => _WriteCalls;

            private int _ReadCalls;
            /// <summary>
            /// Number of Read-calls hacks have done during this states lifetime
            /// </summary>
            public int ReadCalls => _ReadCalls;

            /// <summary>
            /// Gets the total number of memoryinteractions during this states lifetime.
            /// </summary>
            public int TotalRWCalls => _ReadCalls + _WriteCalls;

            private Dictionary<IntPtr, IEntry> Entries;

            public Dictionary<IntPtr, IEntry> FlushState() => Entries;

            public State()
            {
                Entries = new Dictionary<IntPtr, IEntry>();
            }

            /// <summary>
            /// Gets a value from the state if it exists.
            /// </summary>
            /// <typeparam name="T">Type of the struct</typeparam>
            /// <param name="key">Variables key (absolute memory adress)</param>
            /// <param name="data">Out parameter which contains the variable.</param>
            /// <returns>Returns whether the variable exists</returns>
            public bool GetIfExists<T>(IntPtr key, out StateEntry<T> data) where T : struct
            {
                if (Entries.ContainsKey(key))
                {
                    data = (StateEntry<T>)Entries[key];
                    return true;
                }

                data = default(StateEntry<T>);
                return false;
            }

            /// <summary>
            /// Adds a variable to the state. Updates if it already exists.
            /// </summary>
            /// <param name="key">Variables key (absolute memory adress)</param>
            /// <param name="entry">The variable to add</param>
            /// <param name="write">Tells the state if this variable has been tampered with. If true, this variable will be flushed to memory.</param>
            public void AddOrUpdate(IntPtr key, IEntry entry, bool write)
            {
                if (Entries.ContainsKey(key))
                {
                    var val = Entries[key];

                    //makes sure that once a variable has recieved the HasWritten-flag, it stays there.
                    if (write || val.HasWritten)
                        entry.HasWritten = true;

                    Entries[key] = entry;
                }
                else
                    Entries.Add(key, entry);
            }
        }

        
    }
    internal interface IEntry
    {
        bool HasWritten { get; set; }

        Type Type { get; set; }
    }
    internal struct StateEntry<T> : IEntry where T : struct
    {
        public bool HasWritten { get; set; }

        public Type Type { get; set; }

        public T Value { get; set; }
    }
}
